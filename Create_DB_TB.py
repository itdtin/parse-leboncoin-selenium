import mysql.connector
from config import get_sql_data

def create_db(sql_data):
    mydb = mysql.connector.connect(host="{}".format(sql_data['host']), user="{}".format(sql_data['user']), passwd="{}".format(sql_data['psw']))
    mycursor = mydb.cursor()
    mycursor.execute("CREATE DATABASE IF NOT EXISTS {}".format(sql_data['db_name'])) # Try to create DB with db_name
    mycursor.close()
    mydb.close()

def create_table(sql_data):
    mydb = mysql.connector.connect(host="{}".format(sql_data['host']), user="{}".format(sql_data['user']), passwd="{}".format(sql_data['psw']), database = "{}".format(sql_data['db_name']))
    mycursor = mydb.cursor()
    mycursor.execute("CREATE TABLE pages (id INT AUTO_INCREMENT PRIMARY KEY, url VARCHAR(255), source_code LONGTEXT, img1 VARCHAR(255), img2 VARCHAR(255), img3 VARCHAR(255))")
    mydb.close()
    mycursor.close()





def main():
    sql_data = get_sql_data()

    create_db(sql_data)
    create_table(sql_data)


if __name__ == '__main__':
    main()