import csv
from bs4 import BeautifulSoup
from time import sleep
from selenium.common.exceptions import NoSuchElementException
from datetime import datetime
import requests
import re
import mysql.connector
from mysql.connector import Error
from config import get_sql_data, get_headers


def connect_to_db(sql_data):
    """ Connect to MySQL database """
    try:
        print('Connecting to MySQL database...')
        mydb = mysql.connector.connect(host="{}".format(sql_data['host']), user="{}".format(sql_data['user']), passwd="{}".format(sql_data['psw']), database="{}".format(sql_data['db_name']))

        if mydb.is_connected():
            print('connection established.')
            return mydb

    except Error as error:
        print(error)

def pipelines(data):
    data_format = {'url': 'url', 'html': 'html_main', 'img1':'', 'img2':'', 'img3':''}

    for i in data_format:
        try:
            data.update({i:data[i]})
        except:
            data.update({i :''})
    print(data)
    return data




def write_to_db(sql_data, data):
    mydb = connect_to_db(sql_data)
    mycursor = mydb.cursor()

    url = data['url']
    print(url)
    sql = "SELECT * FROM pages WHERE url = '{}'".format(url)
    mycursor.execute(sql)
    rows = mycursor.fetchall()
    mycursor.close()
    if len(rows)==0:
        row = []
        for i in data:
            row.append(data[i])
        row = tuple(row)
        print(row)
        sql = "INSERT INTO pages (url, source_code, img1, img2, img3) VALUES (%s, %s, %s, %s, %s)"
        mycursor2 = mydb.cursor()
        mycursor2.execute(sql, row)
        mydb.commit()

    else:
        print('This url already in database')


def read_csv():
    list = []
    url1 = 'https://www.leboncoin.fr/recherche/?category=9&locations='
    url2 = 'https://www.leboncoin.fr/recherche/?category=10&locations='
    with open('LIST CITIES.csv', 'r') as fp:
        reader = csv.reader(fp, delimiter=',', quotechar='"')
        for row in reader:
            data_row1 = url1 + row[0] + '&page=1'
            data_row2 = url2 + row[0] + '&page=1'
            list.append(data_row1)
            list.append(data_row2)
    return list


# def write_csv(data):
#     row = []
#     for i in data:
#         row.append(data[i])
#     row = tuple(row)
#     with open('locations_data.csv', 'a') as file:
#         writer = csv.writer(file)
#         writer.writerow(row)


def next_page(soup):
    try:
        url = soup.find('ul', class_='_25feg').find_all('li')
        next_url = url[-1].find('a').get('href')
    except NoSuchElementException:
        next_url = ''
    else:
        next_url = 'https://www.leboncoin.fr' + next_url
    return next_url

def parse_ad(link, s):
    link = 'https://www.leboncoin.fr' + link.get('href')
    r = s.get(link)
    html = r.text.strip()
    soup = BeautifulSoup(html, 'lxml')
    try:
        img_links = soup.find_all('span', class_='Lqamr')
    except NoSuchElementException:
        images = []
        print('There are no photo')
    else:
        images = []
        for i in img_links:
            link_text = i.find('div', attrs={'style':True}).get('style')
            img_link = re.sub('background-image\:url\(|background-image\:|\;|\)\;','', link_text)
            images.append(img_link)
        images = images[:3]

    finally:
        data_ad={'url': link,
                 'html':html,}
        for i in range(len(images)):
            data_ad.update({'img{}'.format(i+1): images[i]})
        return data_ad

def parse_all(url, s, sql_data):
    r = s.get(url)
    sleep(5)
    html_main = r.text.strip()
    data = {'url': url,
            'html': html_main}
    data = pipelines(data)
    write_to_db(sql_data, data)
    i = 0
    while True:
        html_main = r.text
        soup = BeautifulSoup(html_main, 'lxml')

        try:
            links = soup.find_all('a',class_='clearfix trackable')
        except:
            links = ''
            i = 1
            print('There are not ads on this page')
        else:

            for link in links:
                data_ad = parse_ad(link, s)
                data = pipelines(data_ad)
                write_to_db(sql_data, data)
        finally:
            url = next_page(soup)
            if url != '':
                r = s.get(url)
            else:
                break

def session_init():
    s = requests.Session()
    headers = get_headers()
    

    s.headers.update(headers)
    return s


def main():


    sql_data = get_sql_data()
    start = datetime.now()
    print('start')
    s = session_init()

    list = read_csv()
    for url in list:
        parse_all(url,s, sql_data)

    end = datetime.now()
    total = end-start   # замер времени расчет
    print(str(total))


if __name__ == '__main__':
    main()


