import csv
from selenium import webdriver
from bs4 import BeautifulSoup
from time import sleep
from selenium.common.exceptions import NoSuchElementException
from datetime import datetime

def read_csv():
    list = []
    url1 = 'https://www.leboncoin.fr/recherche/?category=9&locations='
    url2 = 'https://www.leboncoin.fr/recherche/?category=10&locations='
    with open('LIST CITIES.csv', 'r') as fp:
        reader = csv.reader(fp, delimiter=',', quotechar='"')
        for row in reader:
            data_row1 = url1 + row[0] + '&page=1'
            data_row2 = url2 + row[0] + '&page=1'
            list.append(data_row1)
            list.append(data_row2)
    return list

def write_csv(data):
    row = []
    for i in data:
        row.append(data[i])
    row = tuple(row)
    with open('locations_data.csv', 'a') as file:
        writer = csv.writer(file)
        writer.writerow(row)

def next_page(driver):
    try:
        next_url = driver.find_element_by_xpath('//ul[@class="_25feg"]/li[last()]/a')
    except NoSuchElementException:
        next_url = ''
    else:
        next_url = 'https://www.leboncoin.fr' + next_url
        print(next_url)
    return next_url

def parse_ad(link, driver):
    link = 'https://www.leboncoin.fr' + link.get('href')
    driver.get(link)
    sleep(1)
    try:
        button = driver.find_element_by_xpath('//span[@class="Lqamr"][1]')
    except NoSuchElementException:
        print('There are no slider')
    else:
        button.click()
    html = driver.page_source
    try:
        img_links = driver.find_elements_by_xpath('//div[@class="_2x8BQ"]/img')
    except NoSuchElementException:
        images = []
        print('There are no photo')
    else:
        images = []
        for i in img_links:
            images.append(i.get_attribute("src"))
    finally:
        data_ad={'link': link,
                 'html': html,}
        a = len(images)
        for i in range(len(images)):
            data_ad.update({'image{}'.format(i+1): images[i]})
        return data_ad

def parse_all(url):

    driver = webdriver.Firefox()
    driver.get(url)

    html_main = driver.page_source
    data = {'url': url,
            'html_main': html_main}
    write_csv(data)


    i = 0
    while True:
        html_main = driver.page_source
        soup = BeautifulSoup(html_main, 'lxml')

        try:
            links = soup.find_all('a',class_='clearfix trackable')
        except:
            links = ''
            i = 1
            print('There are not ads on this page')
        else:
            # options = Options()
            # options.add_argument("headless")
            wd = webdriver.Firefox()
            for link in links:
                data_ad = parse_ad(link, wd)
                write_csv(data_ad)
            wd.close()
        finally:
            url = next_page(driver)
            if url != '':
                driver.get(url)
            else:
                break

    driver.close()



def main():

    start = datetime.now()

    list = read_csv()
    for url in list:
        parse_all(url)

    end = datetime.now()
    total = end-start
    print(str(total))




if __name__ == '__main__':
    main()


