import csv
from bs4 import BeautifulSoup
from time import sleep
from datetime import datetime
import requests
import re



def read_csv():
    list = []
    url1 = 'https://www.leboncoin.fr/recherche/?category=9&locations='
    url2 = 'https://www.leboncoin.fr/recherche/?category=10&locations='
    with open('LIST CITIES.csv', 'r') as fp:
        reader = csv.reader(fp, delimiter=',', quotechar='"')
        for row in reader:
            data_row1 = url1 + row[0] + '&page=1'
            data_row2 = url2 + row[0] + '&page=1'
            list.append(data_row1)
            list.append(data_row2)
    return list

def write_csv(data):
    row = []
    for i in data:
        row.append(data[i])
    row = tuple(row)
    with open('locations_data.csv', 'a') as file:
        writer = csv.writer(file)
        writer.writerow(row)

def next_page(soup):
    url = soup.find('ul', class_='_25feg').find_all('li')
    try:
        next_url = url[-1].a['href']
    except TypeError:
        next_url = 'ddd'
    else:
        next_url = 'https://www.leboncoin.fr' + next_url
        print(next_url)
    return next_url

def parse_ad(link, s):
    link = 'https://www.leboncoin.fr' + link.get('href')
    r = s.get(link)
    html = r.text.strip()
    soup = BeautifulSoup(html, 'lxml')
    try:
        img_links = soup.find_all('span', class_='Lqamr')
    except NoSuchElementException:
        images = []
        print('There are no photo')
    else:
        images = []
        for i in img_links:
            link_text = i.find('div', attrs={'style':True}).get('style')
            img_link = re.sub('background-image\:url\(|background-image\:none\;|\)\;', '', link_text)
            if img_link != '':
                images.append(img_link)
            else:
                continue
    finally:
        data_ad={'link': link,
                 'html':'html',}
        a = len(images)
        for i in range(len(images)):
            data_ad.update({'image{}'.format(i+1): images[i]})
        return data_ad

def parse_all(url, s):
    r = s.get(url)
    sleep(5)
    html_main = r.text
    data = {'url': url,
            'html_main': 'html_main'}
    write_csv(data)
    i = 0
    while True:
        html_main = r.text
        soup = BeautifulSoup(html_main, 'lxml')

        try:
            links = soup.find_all('a',class_='clearfix trackable')
        except:
            links = ''
            i = 1
            print('There are not ads on this page')
        else:

            for link in links:
                data_ad = parse_ad(link, s)
                write_csv(data_ad)
        finally:
            url = next_page(soup)
            if url != '':
                r = s.get(url)
            else:
                break

def session_init():
    s = requests.Session()

    cookie = 'cikneeto_uuid=id:06995903-0f0d-4aa5-8e85-66f6adfa304d; _gcl_au=1.1.360514549.1556117487; consent_allpurpose=cDE9MjtwMj0yO3AzPTI7cDQ9MjtwNT0y; cookieBanner=0; oas_ab=a; xtvrn=$562498$; xtan562498=-undefined; xtant562498=1; _fbp=fb.1.1556117496721.832769575; _pulse2data=ee7740c7-bc5f-43ab-96c1-d4c8f2b237c3%2Cv%2C%2C1556118400846%2CeyJpc3N1ZWRBdCI6IjIwMTktMDQtMjRUMTQ6NTE6NDBaIiwiZW5jIjoiQTEyOENCQy1IUzI1NiIsImFsZyI6ImRpciIsImtpZCI6IjIifQ..ZQlgSIoLyPAtPDQE0c5RyA.sK-v1f5npdv_Ia8qZTCqiK_wxyrHJ59Ozv3CXZchVcTb2oa93oHdH_66IOKatul4c0mg-2roa6KCQwSKLxzuXK4ZDpomM5s0BpldCPsex4lnZBa4LkZvrRHl4_LH39xrWap3RYgHHPxi--lwfIFQF8j_Bw9_raa1HsMO1N1bCkWRFQ-pk5WSRDI2ibIuO9DQGixhTgsMBUza55r-AvMl-w.rDwJg2zcpUjEhrivRkEQkQ%2C%2C0%2Ctrue%2C%2CeyJraWQiOiIyIiwiYWxnIjoiSFMyNTYifQ..nGYuMQm2WOTiTZQ6AwJ_MYf4udeWI-8F9slGk05YQuA; trc_cookie_storage=taboola%2520global%253Auser-id%3D8e12cd7c-4ecb-48eb-8f06-1fc428e02b18-tuct3b9fd82; ABTasty=uid%3D19042410512737969%26fst%3D1556117487104%26pst%3Dnull%26cst%3D1556117487104%26ns%3D1%26pvt%3D2%26pvis%3D2%26th%3D422021.538765.2.2.1.1.1556117487135.1556117515772.1; cikneeto=date:1556117515849; utag_main=v_id:016a4fd498d10022d8ba2d62e8060307900530710093c$_sn:1$_ss:0$_pn:2%3Bexp-session$_st:1556119319478$ses_id:1556117493969%3Bexp-session; ABTastySession=sen%3D6__referrer%3D__landingPage%3Dhttps%3A//www.leboncoin.fr/recherche/%3Fcategory%3D9%26locations%3DAubervilliers_93300%26page%3D1__referrerSent%3Dtrue; datadome=._Ssk47MEkCXnPK-0b5YWklJAF6LBcCuEdi23kWzbQM'
    accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3'
    accept_encoding =  'gzip, deflate, br'
    accept_language = 'en-US,en;q=0.9'
    cache_control = 'max-age=0'
    connection = 'keep-alive'
    host = 'www.leboncoin.fr'
    upgrade_insecure_requests = '1'
    h = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36',
         'Accept' : accept,
         'Accept-Encoding' : accept_encoding,
         'Accept-Language' : accept_language,
         'Cache-Control' : cache_control,
         'Connection' : connection,
         'Cookie' : cookie,
         'Host' : host,
         'Upgrade-Insecure-Requests' : upgrade_insecure_requests}

    s.headers.update(h)
    return s


def main():


    start = datetime.now()
    s = session_init()

    list = read_csv()
    for url in list:
        parse_all(url,s)

    end = datetime.now()
    total = end-start   # замер времени расчет
    print(str(total))


if __name__ == '__main__':
    main()


